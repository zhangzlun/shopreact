const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');


/**
 * webpack                       -- 執行 webpack
 * webpack --watch               -- webpack 在你每次變更儲存檔案後自動執行 build
 * webpack --config webpack.config.js  -- 自訂的 webpack 設定檔
 */

module.exports = {

    mode: 'development', //'development' or 'production'

    entry: {
        app: path.resolve(__dirname, 'src/index.js')
    },

    output: {
        filename: 'js/[name].js',
        path: path.resolve(__dirname, 'build/')
    },

    resolve: {
        extensions: ['.js', '.jsx']
    },

    plugins: [
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: "css/[name].css",
            path: __dirname + '/build/css'
        }),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, 'public/index.html'),
            filename: path.resolve(__dirname, 'build/index.html')
        }),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, 'public/test/index.html'),
            filename: path.resolve(__dirname, 'build/test.html')
        })
    ],

    module: {
        rules: [
            // babel-loader
            {
                test: /\.js?$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            // you can specify a publicPath here
                            // by default it use publicPath in webpackOptions.output
                            publicPath: '../t'
                        }
                    },
                    "css-loader"
                ]
            },
            {
                test: /\.(scss|sass)$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            publicPath: '../t'
                        }
                    },
                    "css-loader",
                    {
                        loader:'sass-loader'
                    }
                ]
            },
            {
                test: /\.(png|jpg)$/,
                loader: 'url-loader'
            }
        ]
    },

    optimization: {

        splitChunks: {
            chunks: "initial",
            minChunks: 1,
            cacheGroups: {}
        }
    }


};