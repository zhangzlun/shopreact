import React, {Component} from 'react';
import { Observable, Subject, ReplaySubject, from, of, range } from "rxjs";
import { ajax } from 'rxjs/ajax';
import { map, catchError } from 'rxjs/operators';

class App extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        let test = [1,2,3,4,5];
        let arr = [];

        ajax.getJSON("http://127.0.0.1:8818/api/getSportBetRecords").subscribe(e=>console.log(e));


        for(let vl of test){
            arr.push(<h1>{vl}</h1>);
        }
        return <div>{arr}</div>;
    }

}

export default App;