import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';

import './css/bootstrap.min.css';
import './scss/style.scss';

import './util/all.min';



ReactDOM.render(
    <App />,
    document.getElementById('root')
);